BENCHMARKS=calcpi calcprimes

CC=gcc
CFLAGS=-I.
LIBS=-lm
OPENMP=-fopenmp

all:	$(BENCHMARKS)

calcpi: calcpi.o
	$(CC) $(OPENMP) -o $@ $^ $(CFLAGS) $(LIBS)

calcprimes: calcprimes.o
	$(CC) $(OPENMP) -o $@ $^ $(CFLAGS) $(LIBS)

%.o: %.c
	$(CC) $(OPENMP) -c -o $@ $< $(CFLAGS)

benchmarks: $(BENCHMARKS)
	/bin/bash run-benchmarks.sh

clean:
	rm -f *.o $(BENCHMARKS) *~
