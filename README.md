# OpenMP Benchmarks

Examples of OpenMP shared memory parallel algorithm implementations.  The purpose of
this repository is two fold.  The example implementations should be useful of examples
of writing shared memory OpenMP parallel code on a linux 64 bit system using Gnu C/C++
tools.  Also the examples are meant to be run with a selection of cores/threads for
the OpenMP worker tasks so that you can benchmark the performance of your
OpenMP environment for example in a VM container or other setup.

## Getting Started

If you have suggestions for good OpenMP exampales or benchmarks that would be useful
for this project, please add an issue or make a push request if  you have a working
example.  I am always looking to expand the collection to make it more useful.

### Prerequisites

These OpenMP benchmark examples were created using a standard Linux (Ubuntu 18.04)
environemnt with build-essential Gnu tools and the OpenMP development library
installed.  You should have a minimal environment in a debian based apt package
management system by doing the following:

```
apt -y build-essential libomp-dev
```

### Installing

Cloning the head of the repository should be sufficient to install the example
benchmarks on a system that has an adequate c compiler with OpenMP support.

```
git clone https://dharter@bitbucket.org/dharter/openmp-benchmarks.git
```

## Compiling and Running the Benchmarks

A simple Makefile is provided as an example of how to compile the
OpenMP programs.  You can compile all of the examples by changing
to your repository and invoking make.

```
make all
```

All of the benchmarks take parameters that allow you to specify
properties of the calculation to perform, and the number of OpenMP
threads you should run the calculation with.  You can run all of the
OpenMP benchmarks by invoking the run-benchmarks.sh script:

```
make benchmarks
```

The example programs have been configured to output a summary
of the benchmarks runs in a suitable comma separated format.
You can use standard unix command line tools to gather these
csv results, which would be suitable for plotting or data
analysis in a python/R or spreadsheet:

```
make benchmarks | grep -E 'calcpi|calcprimes' > benchmark-results.csv
```

An example of a complete build and run of all of the example
OpenMP benchmarks should look like the following:

```
ubuntu@tstomp:~/openmp-benchmarks$ make clean
rm -f *.o calcpi calcprimes *~
ubuntu@tstomp:~/openmp-benchmarks$ make
gcc -fopenmp -c -o calcpi.o calcpi.c -I.
gcc -fopenmp -o calcpi calcpi.o -I. -lm
gcc -fopenmp -c -o calcprimes.o calcprimes.c -I.
gcc -fopenmp -o calcprimes calcprimes.o -I. -lm
ubuntu@tstomp:~/openmp-benchmarks$ make benchmarks
/bin/bash run-benchmarks.sh
Calculate PI results (integration by parts)
-------------------------------------------
number of threads  : 1
number of rects 2^n: 30
total time (sec)   : 15.73314
pi approximation   : 3.14159265358979280049513127526239487
math library M_PIl : 3.14159265358979323851280895940618620
error              : 4.3802e-16
calcpi, 1, 30, 15.73314, 4.38018e-16, 3.14159265358979280049513127526239487

Calculate PI results (integration by parts)
-------------------------------------------
number of threads  : 2
number of rects 2^n: 30
total time (sec)   : 7.82231
pi approximation   : 3.14159265358979383937765295087274353
math library M_PIl : 3.14159265358979323851280895940618620
error              : 6.0086e-16
calcpi, 2, 30, 7.82231, 6.00865e-16, 3.14159265358979383937765295087274353

Calculate PI results (integration by parts)
-------------------------------------------
number of threads  : 4
number of rects 2^n: 30
total time (sec)   : 4.07521
pi approximation   : 3.14159265358979302037133185532269408
math library M_PIl : 3.14159265358979323851280895940618620
error              : 2.1814e-16
calcpi, 4, 30, 4.07521, 2.18141e-16, 3.14159265358979302037133185532269408

Calculate PI results (integration by parts)
-------------------------------------------
number of threads  : 8
number of rects 2^n: 30
total time (sec)   : 3.35088
pi approximation   : 3.14159265358979303663436444260526059
math library M_PIl : 3.14159265358979323851280895940618620
error              : 2.0188e-16
calcpi, 8, 30, 3.35088, 2.01878e-16, 3.14159265358979303663436444260526059

Calculate PI results (integration by parts)
-------------------------------------------
number of threads  : 16
number of rects 2^n: 30
total time (sec)   : 1.75391
pi approximation   : 3.14159265358979329033767280421329815
math library M_PIl : 3.14159265358979323851280895940618620
error              : 5.1825e-17
calcpi, 16, 30, 1.75391, 5.18249e-17, 3.14159265358979329033767280421329815

Find primes (using sieve of Eratosthenes algorithm)
---------------------------------------------------
number of threads      : 1
search range (maxPrime): 2 to 1000000000
total time (sec)       : 41.358822
number of primes found : 50847533
calcprimes, 1, 1000000000, 41.358822, 50847533

Find primes (using sieve of Eratosthenes algorithm)
---------------------------------------------------
number of threads      : 2
search range (maxPrime): 2 to 1000000000
total time (sec)       : 29.061094
number of primes found : 50847533
calcprimes, 2, 1000000000, 29.061094, 50847533

Find primes (using sieve of Eratosthenes algorithm)
---------------------------------------------------
number of threads      : 4
search range (maxPrime): 2 to 1000000000
total time (sec)       : 20.137042
number of primes found : 50847533
calcprimes, 4, 1000000000, 20.137042, 50847533

Find primes (using sieve of Eratosthenes algorithm)
---------------------------------------------------
number of threads      : 8
search range (maxPrime): 2 to 1000000000
total time (sec)       : 14.361035
number of primes found : 50847533
calcprimes, 8, 1000000000, 14.361035, 50847533

Find primes (using sieve of Eratosthenes algorithm)
---------------------------------------------------
number of threads      : 16
search range (maxPrime): 2 to 1000000000
total time (sec)       : 12.026219
number of primes found : 50847533
calcprimes, 16, 1000000000, 12.026219, 50847533

```

## Authors
- Derek Harter derek@harter.pro

## License

## Acknowledgements
