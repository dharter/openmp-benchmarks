/**
 * @author Derek Harter
 * @date   May 29, 2019
 *
 * @description Described as the "HelloWorld" of parallel computing.
 *   Calculate pi using integration by parts to approximate pi.  We
 *   are using the formula:
 *
 *   $ \int_0^1 \frac{4.0}{1 + x^2} dx = \pi $
 *
 *   to approximate pi.  We break the interval up into 2^n even
 *   rectangular slices where n is a parameter of our calculation, and
 *   calculate the approximation by summing up the area of these 2^n
 *   rectangles.  We parallelize the algorithm by dividing up the
 *   rectangle calculations evenly among the p threads available to
 *   openmp for calculating.
 *
 *   We use long double floating types, which should give us 128
 *   bit floating point variables, so we can calculate as many
 *   digits of precision as possible.  This should allow approximately
 *   34 decimal digits of precision to be calculated.
 */
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#define __USE_GNU // gnu extensions on linux 64bit allows long double constants
#include <math.h>
#include <assert.h>

// we use long double which are 128 bit native floating point representations
// to get the maximum precision in our calculations of pi possible without
// resorting to a non native data type representation
typedef long double f128;
typedef unsigned int uint;

/** calcpiWorker
 * Calculate an approximation of pi on a subset of rectangles
 * based on our thread id and the number of threads being
 * used in the program.  We will sum up the area of that subset
 * of rectangles under the curve and return that partial sum
 * as our result.
 * 
 * @param threadId - Our thread id ouf ot numThreads running threads
 * @param numThreads - The number of parallel threads being run
 *   to do the work.
 * @param numRectangles - The number of rectangular slices that
 *   the function is being divided into.
 * @param rectangleWidth - The width of each rectangular slice.
 * 
 * @returns f128 Returns the partial sum for the area of the
 *   rectangles we were assigned to calculate.
 */
f128 calcpiWorker(int threadId, int numThreads, uint numRectangles, f128 rectangleWidth)
{
  // keep running sum of areas, at the end this is our approximation of pi
  f128 partialAreaSum = 0.0;  

  // used when calculating area of each rectangular slice
  f128 rectangleHeight;
  f128 x;
  uint numWorkerRectangles = numRectangles / numThreads;
  uint beginRectangle = threadId * numWorkerRectangles;
  uint endRectangle = beginRectangle + numWorkerRectangles;

  // calculate the area of our assigned rectangles
  for (uint rectangle = beginRectangle; rectangle < endRectangle; rectangle++)
  {
    // x position of the middle of this rectangular slice
    x = ((f128)rectangle + 0.5) * rectangleWidth;

    // calculate height of rectangle at the given x position
    rectangleHeight = 4.0 / (1.0 + x * x);

    // add this height to our running sum
    // since the width is constant we sum up the heights and then multiply only 1 time
    // by the width to get the area we are calculating
    //partialAreaSum += rectangleWidth * rectangleHeight;
    partialAreaSum += rectangleHeight;
  }

  // need to do final multiplication of width to get the area
  partialAreaSum *= rectangleWidth;
  
  return partialAreaSum;
}


/** calcpi
 * Calculate an approximation of pi using integration by parts.
 * We divide a function whose area from 0 to 1 integrates to
 * the value of pi into a number of rectangular slices, calculate
 * the area of the small rectangular slices, and sum up these areas
 * to get our approximaiton for pi.
 *
 * @param n - We will divide the function into 2^n rectangular
 *   slices.
 * @param numThreads - The number of parallel threads/openmptasks to
 *   use.  Using p=1 will cause a serial solution to be calculated.
 *   We use a power of 2^n rectangles so we can assign equal
 *   amount of work to threads, thus p should be a power of
 *   2, e.g. p=1,2,4,8,16
 */
void calcpi(int n, int numThreads)
{
  // time how long it takes to do the calculation
  double start = omp_get_wtime();
  
  // number of rectangular slices will be 2^n, and thus each rectangle
  // width will be 1/(2^n)
  uint numRectangles = pow(2, n); // 2^20 approximately 10^6 rectangles
  f128 rectangleWidth = 1.0 / (f128) numRectangles;

  // the final result will be gathered in this shared memory variable
  f128 piApprox = 0.0;
  
  // each parallel thread is responsible for calculating 1/numThreads
  // of the rectangular areas, and each returns their partial
  // area sum as the result
  omp_set_num_threads(numThreads);
  #pragma omp parallel
  {
    // have each worker thread calculate their partial sum on their assigned
    // rectangular slices
    f128 partialAreaSum;
    int threadId = omp_get_thread_num();
    int numActualThreads = omp_get_num_threads();
    assert(numActualThreads == numThreads);
    partialAreaSum = calcpiWorker(threadId, numThreads, numRectangles, rectangleWidth);

    // use a synchronization (atomic mutual exclusion) to ensure that
    // we correctly add together the partial sums calculated without
    // race conditions
    #pragma omp atomic
    piApprox += partialAreaSum;
  }

  // calculate some other final statistics, the time and absolute error achieved
  double stop = omp_get_wtime();
  double time = stop - start;
  f128 error = fabsl(piApprox - M_PIl);
  
  // display results of our calculation
  printf("Calculate PI results (integration by parts)\n");
  printf("-------------------------------------------\n");
  printf("number of threads  : %d\n", numThreads);
  printf("number of rects 2^n: %d\n", n);
  printf("total time (sec)   : %0.5f\n", time);
  printf("pi approximation   : %0.35Lf\n", piApprox);
  printf("math library M_PIl : %0.35Lf\n", M_PIl);
  printf("error              : %0.4Le\n", error);
  printf("calcpi, %d, %d, %0.5f, %0.5Le, %0.35Lf\n", numThreads, n, time, error, piApprox);
  printf("\n");
}


/** usage
 * Display a usage message for the program and exit with error.
 */
void usage()
{
  printf("Usage: calcpi n numThreads\n");
  printf("\n");
  printf("  n          - Controls number of rectangular slices used,\n");
  printf("               we use 2^n rectangles\n");
  printf("  numThreads - The number of openmp worker threads to use for\n");
  printf("               calculation.  This needs to be a power of 2,\n");
  printf("               valid values are 1,2,4,8,16...\n");
  exit(1);
}


/** main
 * The main entry point for this program.  We parse command line
 * arguments here and then setup and start the program.
 *
 * @param argc argument count, number of command line arguments
 *   provided to the program when started.
 * @param argv the command line argument 
 *   we expect 2 fixed arguments
 *   argv[1] == n, to determine number of rectangular slices 2^n
 *   argv[2] == numThreads to control the number of openmp worker threads
 *
 * @returns retuns 0 for successful completion, or non-zero 
 *   if an error occurs.
 */
int main(int argc, char** argv)
{
  int n;
  int numThreads;
  
  // parse command line arguments, check usage first
  if (argc != 3)
  {
    printf("Error: command line arguments missing\n");
    usage();
  }

  // maximum we can handle using regular unsigned int is 2^32
  n = atoi(argv[1]);
  if (n < 1 || n > 32)
  {
    printf("Error: unreasonable number of rectangles specified]\n");
    usage();
  }
  
  numThreads = atoi(argv[2]);
  if (numThreads != 1 &&
      numThreads != 2 &&
      numThreads != 4 &&
      numThreads != 8 &&
      numThreads != 16)
  {
    printf("Error: numThreads must be 1,2,4,8,16\n");
    usage();
  }
  
  // do the actual work
  calcpi(n, numThreads);
  
  // successfully finished calculation
  return 0;
}
