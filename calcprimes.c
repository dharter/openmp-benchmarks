/**
 * @uthor Derek Harter
 * @date May 29, 2019
 *
 * @description Finds the number of primes between 2 and n; uses the
 *   Sieve of Eratosthenes, deleting all multiples of 2, all multiples
 *   of 3, all multiples of 5, etc.; NOT EFFICIENT
 *
 *   Modified from this original source: http://heather.cs.ucdavis.edu/~matloff/OpenMP/Examples/NM/
 */
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>


// unsigned long int to use 64 bit integers, will allow us to search for
// maximum possible range of primes using machine defined int types
// this type should be used for all indexes like the range 2 to maxPrime
// that we search
typedef unsigned long int ulint;

// we really only need a boolean or single bit for our main prime[]
// data structure.  But we avoid doing bit arrays here but at least
// use a short integer so we can potentially use more memory.
// The prime[] array should be an usint, or ultimately turn into a
// boolean bit.
typedef unsigned char ubyte;

// total number of bases checked, 
int totwork;

// in the end prime[i] = 1 if i is prime, otherwise 0
// holds results of crossed off or not
// we allocate this array dynamically so that we
// can check if we exceed memory capacity or not
// for request.  Array should range from 0 to maxPrime+1
ubyte* prime;

// next sieve multiplier to be used
ulint nextbase;  


/** crossout
 * "crosses out" all odd multiples of k, from k*k on, k should always
 * be an odd number, since we begin first by eliminating all even
 * numbers from consideration as none of them are prime.
 *
 * @param base The base of the multiple we are crossing off, starting at k*k.
 * @param maxPrime The limit of the range of primes from 2 to maxPrime
 *   we are searching within.
 */
void crossout(ulint base, ulint maxPrime)
{
  ulint step = 2 * base;
  for (ulint num = base * base; num <= maxPrime; num += step)
  {
    prime[num] = 0;
  }
}


/** calcPrimesWorker
 * Each omp thread does its assigned work in this function.
 *
 * @param maxPrime The limit of the range we are searching for primes,
 *   we search for primes in the range from 2 up to maxPrime.
 *
 * @returns int Returns the amount of work we did in terms of the
 *   number of base numbers we do crossouts on.
 */
int calcPrimesWorker(ulint maxPrime)  
{
  int me;
  int nth;
  int work;
  ulint lim, base;

  me = omp_get_thread_num();
  nth = omp_get_num_threads();
  //printf("worker tid=%d   out of # threads: %d\n", me, nth);

  // no need to check multipliers bigger than sqrt(n)
  lim = sqrt(maxPrime);
  
  // keep track of how much work this thread does
  work = 0;  
  do
  {
    // get next sieve multiplier, avoiding duplication across threads
    // by placing an OpenMP critical section here
    #pragma omp critical
    {
      base = nextbase += 2;
    }
    
    if (base <= lim)
    {
      // record one more base done by this thread
      work++;
      
      // don't bother with crossing out if base is a known composite
      if (prime[base])
      {
	crossout(base, maxPrime);
      }
    }
    else
    {
      return work;
    }
    
  }
  while (1);
}


/** calcPrimes
 * The function to do the actual work.  We set up memory, initialize things
 * and set up environment for workers to do their calculations separately
 * and in parallel.
 *
 * general notes:  
 *
 * The "parallel" clause says, "Have each thread do this block"
 * (enclosed by braces); code not set up by a "parallel" pragma is done
 * only by the master thread
 *
 * on some platforms, might need #pragma omp flush at the end of
 * crossout()
 *
 * @param maxPrime The range to search for primes, from 2 to n.
 * @param numThreads The total number of openmp worker threads to
 *   use for the calculation.
 */
void calcPrimes(ulint maxPrime, int numThreads)
{
  // time how long it takes to do the calculation
  double start = omp_get_wtime();
  
  // assume all numbers are prime until shown otherwise
  // we could potentially parallelize these 2 beginning
  // initializations as well.
  for (ulint num = 2; num <= maxPrime; num++)
  {
    prime[num] = 1;
  }
  
  // cross out the evens separately
  for (ulint num = 2; num <= maxPrime; num += 2)
  {
    prime[num] = 0;
  }
  nextbase = 1;

  // split into parallel worker threads
  omp_set_num_threads(numThreads);
  #pragma omp parallel 
  {
    assert(omp_get_num_threads() == numThreads);
    totwork = calcPrimesWorker(maxPrime); 
    //printf("%d values of base done\n",totwork);
  }
  
  // back to single thread now; report results; no need for barrier, as
  // all threads must have finished for us to reach this point
  int nprimes = 0;
  for (ulint num = 2; num <= maxPrime; num++)
  {
    if (prime[num])
    {
      nprimes++;
    }
  }

  // calculate how long the calculation took
  double end = omp_get_wtime();
  double time = end - start;

  // display result of the calculation
  printf("Find primes (using sieve of Eratosthenes algorithm)\n");
  printf("---------------------------------------------------\n");
  printf("number of threads      : %d\n", numThreads);
  printf("search range (maxPrime): 2 to %lu\n", maxPrime);
  printf("total time (sec)       : %f\n", time);
  printf("number of primes found : %d\n", nprimes);
  printf("calcprimes, %d, %lu, %f, %d\n", numThreads, maxPrime, time, nprimes);
  printf("\n");
}


/** usage
 * Display a usage message for the program and exit with error.
 */
void usage()
{
  printf("Usage: calcprimes maxPrime numThreads\n");
  printf("\n");
  printf("  maxPrime   - We search for primes in range from 2 to the\n");
  printf("               provided maximum number\n");
  printf("  numThreads - The number of openmp worker threads to use for\n");
  printf("               calculation.  This needs to be a power of 2,\n");
  printf("               valid values are 1,2,4,8,16...\n");
  exit(1);
}


/** main
 * The main entry point for this program.  We parse command line
 * arguments here and then setup and start the program.
 *
 * @param argc argument count, number of command line arguments
 *   provided to the program when started.
 * @param argv the command line argument 
 *   we expect x fixed arguments
 *   argv[1] == 
 *   argv[2] == 
 *
 * @returns retuns 0 for successful completion, or non-zero 
 *   if an error occurs.
 */
int main(int argc, char **argv)
{
  ulint maxPrime;
  int numThreads;
  
  // parse command line arguments, check usage first
  if (argc != 3)
  {
    printf("Error: command line arguments missing\n");
    usage();
  }

  // we want maxPrime to be potentially as large as possible, can't
  // use atoi which only handles unsigned ints, need strtoul() instead.
  //maxPrime = atoi(argv[1]);
  char* eptr; // not used here
  maxPrime = strtoul(argv[1], &eptr, 10);
  if (maxPrime < 0)
  {
    printf("Error: maxPrime must be positive, possible overflow of int representation?");
    usage();
  }
  numThreads = atoi(argv[2]);
  if (numThreads != 1 &&
      numThreads != 2 &&
      numThreads != 4 &&
      numThreads != 8 &&
      numThreads != 16)
  {
    printf("Error: numThreads must be 1,2,4,8,16\n");
    usage();
  }

  // Before doing the actual work, attempt to create a block of memory
  // dynamically that will be big enough to hold the results for the maxPrime
  // we are asked to search within.  This block of memory is global
  // and shared by all openmp worker threads.  If unsuccessful it most likely
  // means we do not have enough onboard memory for the requested calculation.
  prime = (ubyte*) calloc(maxPrime + 1, sizeof(ubyte));
  if (prime == NULL)
  {
    printf("Error: failed to allocate sufficient memory for %lu prime search\n", maxPrime);
    printf("  This is most likely due to insufficient main memory needed for\n");
    printf("  the calculation.\n");
    printf("    sizeof(unsigned short int (ubyte): %ld\n", sizeof(ubyte));
    printf("    sizeof maxPrime                  : %ld\n", sizeof(maxPrime));
    printf("    maxprime                         : %lu\n", maxPrime);
    printf("    total memory needed for calc     : %lu bytes\n", sizeof(ubyte) * maxPrime);
    exit(1);
  }
  
  // do the actual work
  calcPrimes(maxPrime, numThreads);

  // successfully finished calculation
  return 0;
}

