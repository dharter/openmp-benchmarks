#!/bin/bash

# n controls number of rectangular slices for calcpi, where we will
# use 2^n rectangles in the calculation
n=30

# run benchmark for the calculation of pi approximation example
for numThreads in 1 2 4 8 16
do
    ./calcpi ${n} ${numThreads}
done

# run benchmark for the finding primes example
maxPrime=1000000000
for numThreads in 1 2 4 8 16
do
    ./calcprimes ${maxPrime} ${numThreads}
done
